<?php

namespace dragonlhp\zbuilder;

use dragonlhp\zbuilder\command\Publish;
use dragonlhp\zbuilder\util\Resources;
use think\Service;

class ZbuilderService extends Service
{
    public function boot()
    {
        Resources::install(false); //检查资源文件

        $this->commands(['zbuilder:publish' => Publish::class]);
    }
}
