# dragonlhp/zbuilder
> thinkphp6 zbuilder for DophinPHP（海豚PHP）的快速构建表格个表单的构建器

## 安装
```
composer require dragonlhp/zbuilder
```

## 资源及数据库配置
```
php think zbuilder:publish

```
以上操作将会复制静态资源文件到`/public/static/zbuilder`目录下

上传文件地址需要自行修改，后期会改为全部是配置文件


## 资源及数据库配置
```
使用方式参考海豚php官方文档说明




